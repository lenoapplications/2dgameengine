//
//  Tuple.hpp
//  GameEngine
//
//  Created by Filip Ćaćić on 30/11/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#ifndef Tuple_hpp
#define Tuple_hpp

#include <stdio.h>
#include <utility>



template <typename F, typename ...R>
struct ValueHolder : public ValueHolder<R...>{
public:
    F firstValue;
    
    ValueHolder(F firstValue,R... restValue) : ValueHolder<R...>(restValue...),firstValue{firstValue}{};
};

template <typename F>
struct ValueHolder<F>{
    ValueHolder(F firstValue):firstValue{firstValue}{};
    
    F firstValue;
};



template<typename F,typename ...R>
class Tuple {
    
private:
    
    template<int index,typename First,typename ...Rest>
    struct GetValueImpl{
        static auto value(const ValueHolder<First,Rest...> *vH) -> decltype(GetValueImpl<index - 1, Rest...>::value(vH)) {
            return GetValueImpl<index-1,Rest...>::value(vH);
        }
    };
    
    template <typename First,typename ...Rest>
    struct GetValueImpl<0,First,Rest...>{
        static auto value(const ValueHolder<First,Rest...> *vH)  {
            return vH->firstValue;
        }
    };
    
    template<int index,typename First,typename ...Rest>
    struct ChangeValueImpl{
        static void change(ValueHolder<First,Rest...> *vH,First&& valueToAdd){
            ChangeValueImpl<index-1,Rest...>::change(vH,std::forward<First>(valueToAdd));
        }
    };
    
    template <typename First,typename ...Rest>
    struct ChangeValueImpl<0,First,Rest...>{
        static void change(ValueHolder<First,Rest...> *vH,First&& valueToAdd)  {
            vH->firstValue = valueToAdd;
        }
    };

 
protected:
    ValueHolder<F, R...> valueHolder;
    
public:
    Tuple(ValueHolder<F, R...>&& valueHolder): valueHolder{valueHolder} {};
    
    template<int index>
    auto get() -> decltype(GetValueImpl<index, F, R...>::value(&valueHolder)) { //typename Type<index, First, Rest...>::value {
        return GetValueImpl<index, F, R...>::value(&valueHolder);
    }
    template<int index>
    void changeValue(F valueToAdd) { //typename Type<index, First, Rest...>::value {
         ChangeValueImpl<index, F, R...>::change(&valueHolder,std::forward<F>(valueToAdd));
    }
    
};


#endif /* Tuple_hpp */

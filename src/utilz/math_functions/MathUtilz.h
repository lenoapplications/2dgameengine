//
// Created by Filip Ćaćić on 06/02/2019.
//

#ifndef GAMEENGINE_MATHUTILZ_H
#define GAMEENGINE_MATHUTILZ_H
#include <math.h>

float roundFloatToTwoDecimal(float value) {
    float newValue = (int) (value * 100 + .5);
    return (float) newValue / 100;
}

float turnFloatToIntegralOrHalf(float value){
    int intFloat = (int) value;
    float absDecimalPart = abs(intFloat - value);
    float difference = absDecimalPart-0.5f;


    if(difference < 0){
        if ( abs(difference) > 0.2f){
            return intFloat;
        }else{
            return 0.5f + intFloat;
        }
    }else{
        if (abs(difference) < 0.2f){
            return intFloat + 1.0f;
        }else{
            return 0.5f + intFloat;
        }
    }
}

#endif //GAMEENGINE_MATHUTILZ_H

//
//  FielderObjectHolder.hpp
//  GameEngine
//
//  Created by Filip Ćaćić on 30/11/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#ifndef FielderObjectHolder_hpp
#define FielderObjectHolder_hpp

#include <stdio.h>
#include <functional>
#include <iostream>


//template<typename First,typename ... Rest>
//class FielderObjectHolder : public FielderObjectHolder<Rest...> {
//public:
//
//    template<typename A>
//    std::function<A()> returningCallbackFunction(A value){
//        auto function = [value](){
//            return 20;
//        };
//        return function;
//    }
//};
//
//template<typename First>
//class FielderObjectHolder<First>{
//public:
//    template<typename A>
//    std::function<A()> returningCallbackFunction(A value){
//        auto function = [value](){
//            return 20;
//        };
//        return function;
//    }
//
//};


class AbstractFielderObjectHolder{
protected:
    const char* key;
public:
    AbstractFielderObjectHolder(){};
    
    bool initialized = false;
    bool operator != (bool objectIsInitialized){
        return initialized != objectIsInitialized;
    };
    bool operator == (bool objectIsInitialized){
        return initialized == objectIsInitialized;
    };
    AbstractFielderObjectHolder(const char* key): key(key){};
    
    bool checkIfKeyMatches(const char* key){
        return this->key == key;
    }
    
    virtual ~AbstractFielderObjectHolder(){};
};

template<typename T>
class FielderObjectHolder : public AbstractFielderObjectHolder{
public:
    FielderObjectHolder(T value,const char* key): AbstractFielderObjectHolder(key),value(value){
        AbstractFielderObjectHolder::initialized = true;
    };
    T getValue(){
        return value;
    }
    
    ~FielderObjectHolder(){
        std::cout<<"FielderObjectHolder is deleted"<<std::endl;
    }
private:
    const T value;
};



#endif /* FielderObjectHolder_hpp */

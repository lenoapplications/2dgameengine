//
//  Fielder.hpp
//  GameEngine
//
//  Created by Filip Ćaćić on 30/11/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#ifndef Fielder_h
#define Fielder_h

#include <stdio.h>
#include <iostream>
#include "utilz/fielder/fielder_object_holder/FielderObjectHolder.h"



template<int size>
class Fielder{
private:
    int getNextFreeIndex(){
        for (int i = 0; i < size ; ++i){
            if (objectHolders[i] == nullptr){
                return i;
            }
        }
        return 0;
    }
    AbstractFielderObjectHolder* objectHolders[size];
public:
    
    Fielder(){
        for (int i = 0; i < size ; ++i){
            objectHolders[i] = nullptr;
        }
    }
    
    ~Fielder(){
        for (int i = 0; i < size;++i){
            if (objectHolders[i] != nullptr){
                delete objectHolders[i];
                objectHolders[i] = nullptr;
            }
        }
    }
    
    template<typename Value>
    Value getValue(const char* key){
        for (int i = 0; i < size ; ++i){
            if (objectHolders[i] != nullptr){
                FielderObjectHolder<Value>* a = dynamic_cast<FielderObjectHolder<Value>*>(objectHolders[i]);

                if (a!= nullptr && a->checkIfKeyMatches(key)){
                    return a->getValue();
                }
            }
        }
        return NULL;
    }
    
protected:
    
    template<typename V>
    void addValue(const char* key,V value){
        FielderObjectHolder<V>* fielderObjectHolder = new FielderObjectHolder<V>(value,key);
        int nextFreeIndex = getNextFreeIndex();
        objectHolders[nextFreeIndex] = fielderObjectHolder;
    }
    
    
    
};

#endif /* Fielder_hpp */

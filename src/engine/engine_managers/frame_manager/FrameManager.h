//
// Created by Filip Ćaćić on 01/01/2019.
//

#ifndef GAMEENGINE_FRAMEMANAGER_H
#define GAMEENGINE_FRAMEMANAGER_H

#include <iostream>
#include <SDL_timer.h>


class FrameManager {
private:
    int desiredFramePerSec;
    float frameStartTime;
    float currentFrameTime;
    float timePerFrame;

    int frameCounter;
    int numberOfFramesInSecond;
    float oneSecondTimer;


public:
    FrameManager(int desiredFramePerSecond);
    ~FrameManager();

    void changeDesiredFramePerSec(int desiredFramePerSec);
    void mesaureFrameStartTime();
    void mesaureFramePassTime();
    void checkCurrentFrameTime();


    bool checkIfOneSecondPass();
    int getFrameCountInSecond();


};


FrameManager::FrameManager(int desiredFramePerSecond):desiredFramePerSec(desiredFramePerSecond) {
    this->timePerFrame = 1000 / desiredFramePerSec;
    this->oneSecondTimer = SDL_GetTicks();
}

FrameManager::~FrameManager() {
    std::cout<<"FrameManager deleted"<<std::endl;
}

void FrameManager::changeDesiredFramePerSec(int desiredFramePerSec) {
    this->desiredFramePerSec = desiredFramePerSec;
    this->timePerFrame = 1000 / this->desiredFramePerSec;

}

void FrameManager::mesaureFrameStartTime() {
    frameStartTime = SDL_GetTicks();
}

void FrameManager::mesaureFramePassTime() {
    currentFrameTime = SDL_GetTicks() - frameStartTime;

}

void FrameManager::checkCurrentFrameTime() {
    if (timePerFrame > currentFrameTime){
        SDL_Delay(timePerFrame - currentFrameTime);
    }
    ++frameCounter;

}

int FrameManager::getFrameCountInSecond() {
    return numberOfFramesInSecond;
}

bool FrameManager::checkIfOneSecondPass() {
    if ((SDL_GetTicks() - oneSecondTimer) >= 1000){
        numberOfFramesInSecond = frameCounter;
        frameCounter = 0;
        oneSecondTimer = SDL_GetTicks();
        return true;
    }
    return false;

}



#endif //GAMEENGINE_FRAMEMANAGER_H

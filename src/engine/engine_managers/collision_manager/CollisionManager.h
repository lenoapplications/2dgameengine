//
// Created by Filip Ćaćić on 27/01/2019.
//

#ifndef GAMEENGINE_COLLISIONMANAGER_H
#define GAMEENGINE_COLLISIONMANAGER_H

#include <engine/engine_models/entity_component_system/ECSDefinedTypes.h>
#include <engine/engine_models/engine_components/collision_component/CollisionComponent.h>
#include <AsyncCommunicator.h>


class CollisionManager {
private:
    AsyncCommunicator* asyncCommunicator;
    EntityMap * entityMap;

    void checkIterator(int indexOfLastCheckedEntity,EntityModel* entity);
    void checkIfEntitiesCloseEnough(EntityModel *entityOne, EntityModel *entityTwo);

    void allowRefreshingEntities();
    void waitForRefreshToFinish();

protected:

public:
    CollisionManager();
    ~CollisionManager();
    void checkCollision();
    void setEntityMap(EntityMap *entityMap);

};


#endif //GAMEENGINE_COLLISIONMANAGER_H

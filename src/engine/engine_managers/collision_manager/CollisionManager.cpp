//
// Created by Filip Ćaćić on 27/01/2019.
//

#include "CollisionManager.h"

CollisionManager::CollisionManager() {
    asyncCommunicator = AsyncCommunicator::getAsyncCommunicator();
}

CollisionManager::~CollisionManager() {

}

void CollisionManager::setEntityMap(EntityMap *entityMap) {
    this->entityMap = entityMap;
}

void CollisionManager::checkCollision() {
    int indexOfLastCheckedEntity = 0;
    for (auto const& entityGroup : *entityMap){
        for (auto const& entity : *entityGroup.second){
            if (entity->isActive()){
                std::thread t1(&CollisionManager::checkIterator,this,0,entity);
                t1.detach();
//                checkIterator(indexOfLastCheckedEntity,entity);
//                ++indexOfLastCheckedEntity;
            }
        }
    }
    allowRefreshingEntities();
    waitForRefreshToFinish();
}



void CollisionManager::checkIterator(int indexOfLastCheckedEntity,EntityModel *entity) {
    for (auto const& entityGroup : *entityMap){
        for (int i = 0; i < entityGroup.second->size(); ++i){
            EntityModel* entityToCompare = entityGroup.second->at(i);

            if (entity != entityToCompare){
                checkIfEntitiesCloseEnough(entity, entityToCompare);
            }
        }
    }
}


void CollisionManager::checkIfEntitiesCloseEnough(EntityModel *entityOne, EntityModel *entityTwo) {
    CollisionComponent* collisionComponentOne =entityOne->getComponentHolder()->getComponent<CollisionComponent>();
    CollisionComponent* collisionComponentTwo =entityTwo->getComponentHolder()->getComponent<CollisionComponent>();
    BoundingVolumeModel* sphericalBoundOne = collisionComponentOne->getBoundingVolumeModel();
    BoundingVolumeModel* sphericalBoundTwo = collisionComponentTwo->getBoundingVolumeModel();

    Vector2d entityOneCenter = sphericalBoundOne->getCenter();
    Vector2d entityTwoCenter = sphericalBoundTwo->getCenter();
    Vector2d vectorDistance = entityOneCenter.subtract(entityTwoCenter);
    int xDistance = abs(vectorDistance.getX());
    int yDistance = abs(vectorDistance.getY());

    int distance = static_cast<int>(sqrt((xDistance * xDistance) + (yDistance * yDistance)));

    if (distance < 50){
        collisionComponentOne->addNewEntityToCheckCollisions(entityTwo);
        collisionComponentTwo->addNewEntityToCheckCollisions(entityOne);
    }

}
void CollisionManager::allowRefreshingEntities() {
    std::thread::id* mainThreadId = asyncCommunicator->getThreadId("mainThread");
    asyncCommunicator->addFlag(*mainThreadId,"refreshEntities",true);
}

void CollisionManager::waitForRefreshToFinish() {
    std::thread::id* mainThreadId = asyncCommunicator->getThreadId("mainThread");
    asyncCommunicator->addFlag(*mainThreadId,"refreshEntities",true);
    while(asyncCommunicator->getFlagStatus(*mainThreadId,"refreshEntities")){}
}




//
// Created by Filip Ćaćić on 04/01/2019.
//

#include "EventManager.h"

void EventManager::checkSDLEvents() {
    SDL_PollEvent(&sdlEvent);
}

SDL_Event *EventManager::getSDLEvent() {
    return &sdlEvent;
}

//
// Created by Filip Ćaćić on 04/01/2019.
//

#ifndef GAMEENGINE_EVENTMANAGER_H
#define GAMEENGINE_EVENTMANAGER_H


#include <SDL_events.h>

class EventManager {
private:
    SDL_Event sdlEvent;
protected:
public:
    void checkSDLEvents();
    SDL_Event* getSDLEvent();
};


#endif //GAMEENGINE_EVENTMANAGER_H

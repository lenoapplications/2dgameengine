//
// Created by Filip Ćaćić on 09/01/2019.
//

#ifndef GAMEENGINE_RENDERER_H
#define GAMEENGINE_RENDERER_H


#include <SDL_system.h>
#include <engine/engine_models/engine_data_holder/physics_data_holder/vector/Vector2d.h>

class Renderer {
protected:
private:
    SDL_Renderer* sdlRenderer;
public:
    Renderer(SDL_Renderer* sdl_renderer);
    ~Renderer();
    void render(SDL_Texture* texture,SDL_Rect* srcRect,SDL_Rect* destRect );
    void renderPoint(Vector2d&& vector2d );
};


#endif //GAMEENGINE_RENDERER_H

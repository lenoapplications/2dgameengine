//
// Created by Filip Ćaćić on 09/01/2019.
//
#include <iostream>
#include "Renderer.h"

Renderer::Renderer(SDL_Renderer *sdl_renderer) {
    this->sdlRenderer = sdl_renderer;
}

Renderer::~Renderer() {

}


void Renderer::render(SDL_Texture *texture, SDL_Rect *srcRect, SDL_Rect *destRect) {
    SDL_RenderCopy(sdlRenderer,texture,srcRect,destRect);

    //SDL_RenderCopyEx(sdlRenderer,texture,srcRect,destRect,180.0f,);
}

void Renderer::renderPoint(Vector2d&& vector2d) {
    SDL_SetRenderDrawColor(sdlRenderer, 0, 255, 255, SDL_ALPHA_OPAQUE);
    for (int i = 0; i < 50; i++){
        SDL_RenderDrawPoint(sdlRenderer,vector2d.getX(),vector2d.getY()+i);
    }
    SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
}



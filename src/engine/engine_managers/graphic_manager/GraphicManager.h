//
// Created by Filip Ćaćić on 09/01/2019.
//

#ifndef GAMEENGINE_GRAPHICMANAGER_H
#define GAMEENGINE_GRAPHICMANAGER_H

#include <engine/engine_managers/graphic_manager/img_loader/ImgLoader.h>
#include <engine/engine_managers/graphic_manager/renderer/Renderer.cpp>
#include <iostream>
#include <engine/engine_models/engine_data_holder/physics_data_holder/vector/Vector2d.h>

class GraphicManager{
private:
    ImgLoader* imgLoader;
    Renderer* renderer;

public:
    void init(SDL_Renderer* sdlRenderer);
    GraphicManager();
    ~GraphicManager();

    SDL_Texture* loadTexture(const char* filePath);
    void renderTexture(SDL_Texture* texture,SDL_Rect* srcRect,SDL_Rect *destRect);
    void renderPoint(Vector2d&& vector2d);

protected:

};



GraphicManager::GraphicManager() {
}

GraphicManager::~GraphicManager() {
    delete imgLoader;
}

void GraphicManager::init(SDL_Renderer *sdlRenderer) {
    this->imgLoader = new ImgLoader(sdlRenderer);
    this->renderer = new Renderer(sdlRenderer);
}

SDL_Texture *GraphicManager::loadTexture(const char* filePath) {
    return imgLoader->loadTexture(filePath);
}

void GraphicManager::renderTexture(SDL_Texture *texture, SDL_Rect* srcRect, SDL_Rect *destRect) {
    this->renderer->render(texture,srcRect,destRect);
}

void GraphicManager::renderPoint(Vector2d&& vector2d) {
    this->renderer->renderPoint(std::forward<Vector2d>(vector2d));
}


#endif //GAMEENGINE_GRAPHICMANAGER_H

//
// Created by Filip Ćaćić on 09/01/2019.
//

#ifndef GAMEENGINE_IMGLOADER_H
#define GAMEENGINE_IMGLOADER_H


#include <SDL_system.h>
#include <string>
#include <zconf.h>
#include <SDL_image.h>



class ImgLoader {
private:
    SDL_Renderer* sdlRenderer;
    std::string mainPathToAsset;
protected:
public:
    explicit ImgLoader(SDL_Renderer* sdl_renderer);
    ~ImgLoader();

    SDL_Texture* loadTexture(const char* imgFileName);

};


#endif //GAMEENGINE_IMGLOADER_H

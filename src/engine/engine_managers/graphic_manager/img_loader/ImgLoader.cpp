#include <utility>

//
// Created by Filip Ćaćić on 09/01/2019.
//

#include <iostream>
#include "ImgLoader.h"

ImgLoader::ImgLoader(SDL_Renderer* sdl_renderer) {

    char *answer = nullptr;
    int size = 1;
    while(answer == nullptr){
        char buffer[size];
        answer = getcwd(buffer, sizeof(buffer));

        if (answer){
            mainPathToAsset = std::string(answer) + "/asset/";
        }else if (size == 999){
            throw std::out_of_range("IMG LOADER -> to big size for buffer, main path not found");
        }
        size += 2;
    }
    this->sdlRenderer = sdl_renderer;
}

ImgLoader::~ImgLoader() {

}

SDL_Texture *ImgLoader::loadTexture(const char* imgFileName) {
    const std::string concatedString =  mainPathToAsset + imgFileName;

    FILE* file = fopen(concatedString.c_str(),"r");
    if ( file ){
        fclose(file);

        SDL_Surface* surface = IMG_Load(concatedString.c_str());
        SDL_Texture* texture = SDL_CreateTextureFromSurface(sdlRenderer,surface);
        SDL_FreeSurface(surface);
        return texture;
    }else{
        throw std::invalid_argument("File not found");
    }
}


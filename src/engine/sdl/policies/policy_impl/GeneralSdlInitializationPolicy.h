//
// Created by Filip Ćaćić on 04/12/2018.
//

#ifndef GAMEENGINE_GENERALSDLINITIALIZATIONPOLICY_H
#define GAMEENGINE_GENERALSDLINITIALIZATIONPOLICY_H


#include "engine/sdl/policies/sdl_initialization_policies/SdlInitializationPolicy.h"
#include <engine/engine_models/entity_component_system/ecs_initializator_interface/ECSInitializatorInterface.h>
#include <SDL.h>


template<typename ECS>
class GeneralSdlInitializationPolicy : public InitializationPolicy<2>{
private:


public:
    GeneralSdlInitializationPolicy():InitializationPolicy(){
    };
    ~GeneralSdlInitializationPolicy() override {
    };

    void setupECSInitializator(){
        ECS ecs{};
        ecs.initECS(ecsManager,graphicManager);
    }

    bool mainSetup() override {
        if (SDL_Init(SDL_INIT_EVERYTHING) == 0){
            if (createWindow("GeneralSDL",getValue<int>("WIDTH"),getValue<int>("HEIGHT"))){
                if (createRenderer()){
                    std::cout<<"EVERYTING INITIALIZED "<<std::endl;
                    initManagers();
                    return true;
                }
            }
        }
        return false;
    }
    void initFielder()override {
        addValue("WIDTH",640);
        addValue("HEIGHT",480);
    }

    void frameProcess() override {
        if (ecsManager->update()->type == SDL_QUIT){
            engineDeactivated();
        }
    }




};




#endif //GAMEENGINE_GENERALSDLINITIALIZATIONPOLICY_H

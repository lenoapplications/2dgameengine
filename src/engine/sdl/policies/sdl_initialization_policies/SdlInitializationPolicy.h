//
//  SdlInitializationPolicy.h
//  GameEngine
//
//  Created by Filip Ćaćić on 03/12/2018.
//  Copyright © 2018 Filip Ćaćić. All rights reserved.
//

#ifndef SdlInitializationPolicy_h
#define SdlInitializationPolicy_h

#include <engine/sdl/sdl_initializator/SdlEngineInitializator.h>
#include <engine/engine_managers/frame_manager/FrameManager.h>
#include <engine/engine_models/entity_component_system/ecs_manager/ECSManager.cpp>

#include <SDL_system.h>
#include <engine/engine_managers/graphic_manager/GraphicManager.h>
#include "utilz/fielder/Fielder.h"



template<int size>
class InitializationPolicy : public Fielder<size>{
private:
    bool ENGINE_ACTIVE = false;

    SDL_Window* mainSDLWindow;
    SDL_Renderer* mainSDLRenderer;
    FrameManager* frameManager;

    void renderClear(){
        SDL_RenderClear(mainSDLRenderer);
    }
    void presentRender(){
        SDL_SetRenderDrawColor(mainSDLRenderer,255,255,255,1);
        SDL_RenderPresent(mainSDLRenderer);
    }


protected:

    ECSManager* ecsManager;
    GraphicManager* graphicManager;
    bool createWindow(const char* title,int width,int height){
        mainSDLWindow = SDL_CreateWindow(title,200,200,640,480,SDL_WINDOW_SHOWN);
        return mainSDLWindow != nullptr;
    }


    bool createRenderer(){
        mainSDLRenderer = SDL_CreateRenderer(mainSDLWindow,-1,0);
        return mainSDLRenderer != nullptr;
    }

    void initManagers() {
        graphicManager->init(mainSDLRenderer);
    }

public:
    InitializationPolicy(){
      frameManager = new FrameManager(60);
      graphicManager = new GraphicManager();
      ecsManager = new ECSManager();
    };
    virtual ~InitializationPolicy(){
        delete frameManager;
        delete graphicManager;
        delete ecsManager;
    };


    virtual bool mainSetup()= 0;
    virtual void initFielder()=0;
    virtual void frameProcess()=0;
    virtual void setupECSInitializator()=0;



    void runEngine(){
        std::cout<<"Engine starded"<<std::endl;
        ecsManager->setupSecundaryThread();
        while(ENGINE_ACTIVE){
            frameManager->mesaureFrameStartTime();

            renderClear();
            frameProcess();
            presentRender();

            frameManager->mesaureFramePassTime();
            frameManager->checkCurrentFrameTime();

            if (frameManager->checkIfOneSecondPass()){
                std::cout<<"frame passed is "<<frameManager->getFrameCountInSecond()<<std::endl;
            }
        }
    }



    void engineActivated(){
        ENGINE_ACTIVE = true;
    }
    void engineDeactivated(){
        ENGINE_ACTIVE = false;
    }
};

#endif /* SdlInitializationPolicy_h */

//
// Created by Filip Ćaćić on 04/12/2018.
//

#ifndef GAMEENGINE_SDLENGINEINITIALIZATOR_H
#define GAMEENGINE_SDLENGINEINITIALIZATOR_H
#include <iostream>



template<typename I>
class SdlEngineInitializator {
private:
    I initializator;

public:
    SdlEngineInitializator();
    ~SdlEngineInitializator();

    void sdlEngineInitialization();
};


template<typename I>
void SdlEngineInitializator<I>::sdlEngineInitialization() {
    initializator.initFielder();

    if (initializator.mainSetup()){
        initializator.setupECSInitializator();
        initializator.engineActivated();
        initializator.runEngine();
    };

}

template<typename I>
SdlEngineInitializator<I>::SdlEngineInitializator() {

}

template<typename I>
SdlEngineInitializator<I>::~SdlEngineInitializator() {

}


#endif //GAMEENGINE_SDLENGINEINITIALIZATOR_H

//
// Created by Filip Ćaćić on 22/01/2019.
//

#ifndef GAMEENGINE_COLLISIONDETECTORMODEL_H
#define GAMEENGINE_COLLISIONDETECTORMODEL_H


#include <engine/engine_models/entity_component_system/component_model/component_holder/ComponentHolder.h>

class CollisionDetectorInterface {
private:
    EntityModel* entityModel;
protected:

public:
    CollisionDetectorInterface(){};
    ~CollisionDetectorInterface(){};
    virtual void detectCollision(ComponentHolder* componentHolder,EntityModel* entityModel)=0;
    void setEntityModel(EntityModel* entityModel){
        this->entityModel = entityModel;
    };
};


#endif //GAMEENGINE_COLLISIONDETECTORMODEL_H

//
// Created by Filip Ćaćić on 11/01/2019.
//

#ifndef GAMEENGINE_EVENTLISTENERINTERFACE_H
#define GAMEENGINE_EVENTLISTENERINTERFACE_H
#include <SDL_events.h>
class ComponentHolder;

class EventListenerInterface{
private:
protected:
public:
    virtual void handleEvent(SDL_Event* sdlEvent,ComponentHolder* componentHolder)=0;

};

#endif //GAMEENGINE_EVENTLISTENERINTERFACE_H

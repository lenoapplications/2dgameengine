//
// Created by Filip Ćaćić on 01/01/2019.
//

#ifndef GAMEENGINE_DEFINEDVALUES_H
#define GAMEENGINE_DEFINEDVALUES_H
#include <vector>
#include <bitset>
#include <array>
#include <utilz/fielder/Fielder.h>
#include "map"


class ComponentModel;
class EntityModel;

using ComponentId = size_t;
using GroupId = size_t ;

constexpr size_t  maxComponentPerEntity = 32;
constexpr size_t  maxGroups = 32;


using ComponentBitSet = std::bitset<maxComponentPerEntity>;
using ComponentArray = std::array<ComponentModel*,maxComponentPerEntity>;
using EntityMap = std::map<GroupId , std::vector<EntityModel*>*>;
using EntityVector = std::vector<EntityModel*>;



#endif //GAMEENGINE_DEFINEDVALUES_H

//
// Created by Filip Ćaćić on 08/01/2019.
//

#ifndef GAMEENGINE_ECSINITIALIZATORINTERFACE_H
#define GAMEENGINE_ECSINITIALIZATORINTERFACE_H
#include <engine/engine_models/entity_component_system/ecs_manager/ECSManager.h>
#include <engine/engine_managers/graphic_manager/GraphicManager.h>


class ECSInitializatorInterface {
public:
    ECSInitializatorInterface() = default;
    ~ECSInitializatorInterface() = default;
    virtual void initECS(ECSManager* ecsManager,GraphicManager* graphicManager)=0;

};


#endif //GAMEENGINE_ECSINITIALIZATORINTERFACE_H

//
// Created by Filip Ćaćić on 01/01/2019.
//
#include "EntityManager.h"
#include <engine/engine_models/entity_component_system/entity_model/EntityModel.cpp>

EntityManager::EntityManager() {
}

EntityManager::~EntityManager() {
    for (auto const& groupIdIt : *entityMap){
        for (auto const& entity : *groupIdIt.second){
            delete entity;
        }
        groupIdIt.second->clear();
        delete groupIdIt.second;
    }
    entityMap->clear();
    delete entityMap;
}


template <typename Entity>
void EntityManager::addEntity(Entity *entityModel) {
    EntityMap :: iterator groupIterator;
    GroupId id = entityModel->getGroupId();

    if (getGroupIterator(&groupIterator,id)){
        EntityVector* existingEntityGroup = groupIterator->second;
        existingEntityGroup->push_back(entityModel);

    }else{
        EntityVector* newEntityGroup = new EntityVector();
        newEntityGroup->push_back(entityModel);
        entityMap->insert(std::pair<GroupId,EntityVector*>(id,newEntityGroup));
    }
}

bool EntityManager::getGroupIterator(EntityMap ::iterator* itGroup,GroupId groupId) {
    *itGroup = entityMap->find(groupId);
    return *itGroup != entityMap->end();
}




void EntityManager::drawEntities() {
    for (auto const& entityGroup : *entityMap){
        for (auto const& entity : *entityGroup.second){
            if (entity->isActive()){
                entity->draw();
            }
        }
    }
}

void EntityManager::updateEntities() {
    for (auto const& entityGroup : *entityMap){
        for (auto const& entity : *entityGroup.second){
            if (entity->isActive()){
                entity->update();
            }
        }
    }
}


void EntityManager::refreshEntities() {
    for (auto const& entityGroup : *entityMap){
        EntityVector * vector = entityGroup.second;
        int index = 0;
        while(index != vector->size()){

            if (!vector->at(index)->isActive()){
                EntityModel* entityModel = vector->at(index);
                vector->erase(vector->begin()+index);
                delete entityModel;
            }else{
                ++index;
            }
        }
    }
}


template<typename Value>
EntityModel *EntityManager::getEntity(GroupId groupId, const char *key, Value value) {
    for (auto const& groupIdIt : *entityMap){

        if (groupIdIt.first == groupId){
            for ( auto const& entity : *groupIdIt.second){
                Value entityValue = entity->getValue<Value>(key);

                if (entityValue != NULL && entityValue == value){
                    return entity;
                }
            }
        }
    }
    return nullptr;
}


std::vector<EntityModel *> *EntityManager::getGroupEntity(GroupId groupId) {
    return nullptr;
}


void EntityManager::setEntityMap(EntityMap *entityMap) {
    this->entityMap = entityMap;
}



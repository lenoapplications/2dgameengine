//
// Created by Filip Ćaćić on 01/01/2019.
//

#ifndef GAMEENGINE_ENTITYMANAGER_H
#define GAMEENGINE_ENTITYMANAGER_H

#include <iostream>
#include <engine/engine_models/entity_component_system/ECSDefinedTypes.h>



class EntityManager {
private:
    EntityMap* entityMap;

    bool getGroupIterator(EntityMap :: iterator* itGroup,GroupId groupId);
public:
    EntityManager();
    ~EntityManager();

    void setEntityMap(EntityMap* entityMap);
    template<typename Entity>
    void addEntity(Entity* entityModel);
    template<typename Value> EntityModel* getEntity(GroupId groupId,const char* key,Value value);
    std::vector<EntityModel*>* getGroupEntity(GroupId groupId);


    void updateEntities();
    void drawEntities();
    void refreshEntities();

};


#endif //GAMEENGINE_ENTITYMANAGER_H

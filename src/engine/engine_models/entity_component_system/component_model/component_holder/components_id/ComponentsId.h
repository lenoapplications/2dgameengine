//
// Created by Filip Ćaćić on 26/01/2019.
//

#ifndef GAMEENGINE_COMPONENTSID_H
#define GAMEENGINE_COMPONENTSID_H

class SpriteComponent;
class EventComponent;
class ForceComponent;
class CollisionComponent;

template<typename Component>
int getDefinedComponentId(){

};

template <>
int getDefinedComponentId<SpriteComponent>(){
    return 0;
}

template <>
int getDefinedComponentId<EventComponent>(){
    return 1;
}

template <>
int getDefinedComponentId<ForceComponent>(){
    return 2;
}

template <>
int getDefinedComponentId<CollisionComponent>(){
    return 3;
}




#endif //GAMEENGINE_COMPONENTSID_H

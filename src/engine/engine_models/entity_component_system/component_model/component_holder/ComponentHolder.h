//
// Created by Filip Ćaćić on 11/01/2019.
//

#ifndef GAMEENGINE_COMPONENTHOLDER_H
#define GAMEENGINE_COMPONENTHOLDER_H


#include <engine/engine_models/entity_component_system/ECSDefinedTypes.h>
#include <engine/engine_models/entity_component_system/component_model/component_holder/components_id/ComponentsId.h>


class ComponentHolder {
private:
    template <typename T> ComponentId getComponentId();
    void increaseComponentNumber();


    int numberOfComponents;
    ComponentArray components{};
public:
    ComponentHolder();
    ~ComponentHolder();
    template<typename T,typename ...Targs> T* addNewComponent(Targs&& ...args);
    template<typename T> bool hasComponent();
    template <typename T>T* getComponent();


    void update();
    void draw();


};


#endif //GAMEENGINE_COMPONENTHOLDER_H

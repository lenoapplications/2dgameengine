//
// Created by Filip Ćaćić on 11/01/2019.
//

#include "ComponentHolder.h"

ComponentHolder::ComponentHolder(){

}

ComponentHolder::~ComponentHolder() {
    for (auto& c : components)delete c;
    std::cout<<"EntityModel deleted"<<std::endl;
}
template<typename T>
ComponentId ComponentHolder::getComponentId() {
    auto typeId = getDefinedComponentId<T>();
    return typeId;
}

void ComponentHolder::increaseComponentNumber() {
    numberOfComponents++;
}

template<typename T>
T *ComponentHolder::getComponent() {
    if (hasComponent<T>()){
        auto ptr(components[getComponentId<T>()]);
        return static_cast<T*>(ptr);
    }
    return nullptr;
}

template<typename T, typename... Targs>
T* ComponentHolder::addNewComponent(Targs &&... args) {
    ComponentId componentId = getComponentId<T>();
    T* component ( new T(std::forward<Targs>(args)...) );
    components[componentId] = component;
    increaseComponentNumber();
    return component;
}


void ComponentHolder::update() {
    for (int i = 0; i < numberOfComponents; ++i){
        components[i]->update();
    }
}

void ComponentHolder::draw() {
    for (int i = 0; i < numberOfComponents; ++i){
        components[i]->draw();
    }
}

template<typename T>
bool ComponentHolder::hasComponent() {
    return components[getComponentId<T>()] != nullptr;
}



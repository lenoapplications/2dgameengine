//
// Created by Filip Ćaćić on 01/01/2019.
//

#ifndef GAMEENGINE_COMPONENTMODEL_H
#define GAMEENGINE_COMPONENTMODEL_H

#include <iostream>
#include <engine/engine_models/entity_component_system/entity_model/EntityModel.h>



class ComponentModel{
private:
protected:
    EntityModel* entity = nullptr;
public:
    virtual ~ComponentModel() {
        std::cout<<"COMPONENT deleted"<<std::endl;
    }

    void setEntity(EntityModel* entity) {
        this->entity = entity;
    }

    virtual void init()=0;
    virtual void update()=0;
    virtual void draw()=0;

};

#endif //GAMEENGINE_COMPONENTMODEL_H

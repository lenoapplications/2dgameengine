//
// Created by Filip Ćaćić on 01/01/2019.
//

#include "ECSManager.h"

ECSManager::ECSManager() {
    EntityMap* entityMap = new EntityMap();

    entityManager = new EntityManager();
    collisionManager = new CollisionManager();
    eventManager = new EventManager();

    entityManager->setEntityMap(entityMap);
    collisionManager->setEntityMap(entityMap);
    asyncCommunicator = AsyncCommunicator::getAsyncCommunicator();

    setupMainThread();
}

ECSManager::~ECSManager() {
    std::cout<<"Entity manager deleted "<<std::endl;
    std::cout<<"Event manager deleted "<<std::endl;
    delete entityManager;
    delete eventManager;
    delete collisionManager;
}


void ECSManager::setupSecundaryThread() {
    std::thread collisionCheck(&ECSManager::secundaryThreadGameLoop,this);
    secundaryThread = asyncCommunicator->startNewThread(&collisionCheck);
    asyncCommunicator->saveThreadToMap(secundaryThread,"secundaryThread");
}
void ECSManager::setupMainThread() {
    mainThreadId = std::this_thread::get_id();
    asyncCommunicator->saveThreadId(std::this_thread::get_id());
    asyncCommunicator->saveThreadToMap(std::this_thread::get_id(),"mainThread");
    asyncCommunicator->addFlag(std::this_thread::get_id(),"entityAdded",false);
    asyncCommunicator->addFlag(mainThreadId,"newEntityReady",true);
}



template <typename Entity>
void ECSManager::preSetupForEntity(Entity *entityModel) {
    entityModel->insertEventManager(this->eventManager);
    entityModel->insertEntityManager(this->entityManager);
    entityModel->init();
}

template <typename Entity>
void ECSManager::addEntityRuntime(Entity *entityModel) {
    preSetupForEntity(entityModel);
    while(!asyncCommunicator->getFlagStatus(mainThreadId,"newEntityReady")){}
    asyncCommunicator->addObjectToThreadId(mainThreadId,"newEntity",entityModel);
    asyncCommunicator->addFlag(mainThreadId,"newEntityReady",false);
}

template<typename Entity>
void ECSManager::addEntityInit(Entity *entityModel) {
    preSetupForEntity(entityModel);
    entityManager->addEntity(entityModel);
}


SDL_Event* ECSManager::update() {
    eventManager->checkSDLEvents();
    entityManager->updateEntities();
    entityManager->drawEntities();
    checkIfCanRefreshEntities();

    return eventManager->getSDLEvent();

}

void ECSManager::secundaryThreadGameLoop() {
    int a = 2;
    while( a == 2){
        collisionManager->checkCollision();
    }

}


bool ECSManager::waitForEmptySpotBeforeAddingNewEntity() {
    while(asyncCommunicator->doesObjectExist(secundaryThread,"newCollisionEntity")){}
}

void ECSManager::reserverEntitySpot() {
    asyncCommunicator->addFlag(mainThreadId,"entityAdded",true);
}

void ECSManager::checkIfCanRefreshEntities() {
    if (asyncCommunicator->getFlagStatus(mainThreadId,"refreshEntities")){
        entityManager->refreshEntities();
        asyncCommunicator->addFlag(mainThreadId,"refreshEntities",false);
    }
}

void ECSManager::checkForNewEntities() {
    if (asyncCommunicator->doesObjectExist(mainThreadId,"newEntity")){
        EntityModel* entityModel = asyncCommunicator->getObject<EntityModel>(mainThreadId,"newEntity");
        entityManager->addEntity(entityModel);
        asyncCommunicator->addFlag(mainThreadId,"newEntityReady",true);

        waitForEmptySpotBeforeAddingNewEntity();
        asyncCommunicator->addObjectToThreadId(secundaryThread,"newCollisionEntity",entityModel);

        asyncCommunicator->removeObject(mainThreadId,"newEntity");
    }
}





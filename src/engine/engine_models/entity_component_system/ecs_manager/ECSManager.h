//
// Created by Filip Ćaćić on 01/01/2019.
//

#ifndef GAMEENGINE_ECSMANAGER_H
#define GAMEENGINE_ECSMANAGER_H
#include <iostream>
#include <engine/engine_models/entity_component_system/entity_manager/EntityManager.cpp>
#include <engine/engine_managers/event_manager/EventManager.h>
#include <engine/engine_managers/collision_manager/CollisionManager.cpp>
#include <thread>
#include <AsyncCommunicator.h>



class ECSManager {
private:
    AsyncCommunicator* asyncCommunicator;
    EntityManager* entityManager;
    CollisionManager* collisionManager;
    EventManager* eventManager;
    std::thread::id mainThreadId;
    std::thread::id secundaryThread;


    template<typename Entity>
    void preSetupForEntity(Entity* entityModel);
    void setupMainThread();


    void reserverEntitySpot();
    bool waitForEmptySpotBeforeAddingNewEntity();
    void checkForNewEntities();
    void checkIfCanRefreshEntities();

    void secundaryThreadGameLoop();

public:
    ECSManager();
    ~ECSManager();

    template <typename Entity>
    void addEntityRuntime(Entity *entityModel);
    template <typename Entity>
    void addEntityInit(Entity * entityModel);
    SDL_Event* update();
    void setupSecundaryThread();



};


#endif //GAMEENGINE_ECSMANAGER_H

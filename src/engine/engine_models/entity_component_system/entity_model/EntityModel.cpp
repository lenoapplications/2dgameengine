#include <engine/engine_models/engine_components/collision_component/bounding_volumes/bounding_volume_model/spherical_bound/SphericalBound.h>
#include "EntityModel.h"

//
// Created by Filip Ćaćić on 01/01/2019.
//

EntityModel::EntityModel(GraphicManager* graphicManager,GroupId groupId):groupId(groupId),graphicManager(graphicManager) {
    active = true;
    componentHolder = new ComponentHolder();
    engineDataHolder = new EngineDataHolder();
}


EntityModel::~EntityModel() {
    std::cout<<"DELETING ENTITY"<<std::endl;
    delete componentHolder;
    delete engineDataHolder;
}



void EntityModel::update() {
    componentHolder->update();
    SphericalBound sphericalBound (this);

    renderPoint(sphericalBound.test());
}



void EntityModel::draw() {
    componentHolder->draw();
}




template<typename T, typename... Targs>
void EntityModel::addNewComponent(Targs &&... args) {
    T* component = componentHolder->addNewComponent<T>(std::forward<Targs>(args)...);
    component->setEntity(this);
    component->init();
}

template<typename T>
bool EntityModel::hasComponent(){
    return componentHolder->hasComponent<T>();
}

ComponentHolder* EntityModel::getComponentHolder(){
    return componentHolder;
}


GroupId EntityModel::getGroupId() const {
    return groupId;
}


SDL_Event *EntityModel::getSdlEvent(){
    return this->eventManager->getSDLEvent();
}

bool EntityModel::isActive() const{
    return active;
}

void EntityModel::destroy() {
    active = false;
}

void EntityModel::insertEventManager(EventManager* eventManager) {
    this->eventManager = eventManager;
}

void EntityModel::insertEntityManager(EntityManager*entityManager) {
    this->entityManager = entityManager;

}


template<typename Value>
EntityModel *EntityModel::getOtherEntity(GroupId groupId, const char *key, Value value) {
    return this->entityManager->getEntity(groupId,key,value);
}

void EntityModel::render(SDL_Texture *texture, SDL_Rect *srcRect, SDL_Rect *destRect) {
    this->graphicManager->renderTexture(texture,srcRect,destRect);
}

SDL_Texture *EntityModel::loadTexture(const char *filePathName) {
    return this->graphicManager->loadTexture(filePathName);
}

EngineDataHolder *EntityModel::getEngineDataHolder() {
    return engineDataHolder;
}

void EntityModel::renderPoint(Vector2d&& vector2d) {
    this->graphicManager->renderPoint(std::forward<Vector2d>(vector2d));
}









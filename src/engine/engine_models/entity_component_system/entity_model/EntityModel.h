//
// Created by Filip Ćaćić on 01/01/2019.
//

#ifndef GAMEENGINE_ENTITYMODEL_H
#define GAMEENGINE_ENTITYMODEL_H

#include "../ECSDefinedTypes.h"
#include <utilz/fielder/Fielder.h>
#include <iostream>
#include <array>

#include <engine/engine_managers/event_manager/EventManager.cpp>
#include <engine/engine_models/entity_component_system/entity_manager/EntityManager.h>
#include <engine/engine_models/entity_component_system/component_model/ComponentModel.h>
#include <engine/engine_managers/graphic_manager/GraphicManager.h>
#include <engine/engine_models/engine_components/graphics_component/graphic_utilz/graphic_data_holder/GraphicDataHolder.h>
#include <engine/engine_models/entity_component_system/component_model/component_holder/ComponentHolder.cpp>
#include <engine/engine_models/engine_data_holder/EngineDataHolder.h>


class EntityModel : public Fielder<5>{
private:
    const GroupId groupId;
    bool active;
    EntityManager* entityManager;
    EventManager* eventManager = nullptr;
    GraphicManager* graphicManager;
    ComponentHolder* componentHolder;

protected:
    EngineDataHolder* engineDataHolder;

public:
    explicit EntityModel(GraphicManager* graphicManager,GroupId groupId);
    virtual ~EntityModel();

    virtual void init(){};
    void draw();
    void update();

    template<typename T,typename ...Targs> void addNewComponent(Targs&& ...args);
    ComponentHolder* getComponentHolder();
    void insertEventManager(EventManager* eventManager);
    void insertEntityManager(EntityManager* entityManager);
    EngineDataHolder* getEngineDataHolder();

    template<typename T> bool hasComponent();
    bool isActive()const;
    SDL_Event* getSdlEvent();
    GroupId getGroupId()const;
    template<typename Value> EntityModel* getOtherEntity(GroupId groupId,const char* key,Value value);

    SDL_Texture* loadTexture(const char* filePathName);
    void render(SDL_Texture* texture,SDL_Rect* srcRect,SDL_Rect* destRect);
    void renderPoint(Vector2d&& vector2d);

    void destroy();




};

#endif //GAMEENGINE_ENTITYMODEL_H

//
// Created by Filip Ćaćić on 22/01/2019.
//

#ifndef GAMEENGINE_ENGINEDATAHOLDER_H
#define GAMEENGINE_ENGINEDATAHOLDER_H

#include <engine/engine_models/engine_data_holder/graphic_data_holder/GraphicDataHolder.h>
#include <engine/engine_models/engine_data_holder/physics_data_holder/PhysicsDataHolder.h>

class EngineDataHolder{
private:

protected:

public:
    GraphicDataHolder* graphicDataHolder;
    PhysicsDataHolder* physicsDataHolder;

    EngineDataHolder(){
        graphicDataHolder = new GraphicDataHolder();
        physicsDataHolder = new PhysicsDataHolder();
    }
    ~EngineDataHolder(){
        delete graphicDataHolder;
        delete physicsDataHolder;
    }

};

#endif //GAMEENGINE_ENGINEDATAHOLDER_H

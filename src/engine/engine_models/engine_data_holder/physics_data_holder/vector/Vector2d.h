//
// Created by Filip Ćaćić on 11/01/2019.
//

#ifndef GAMEENGINE_VECTOR_H
#define GAMEENGINE_VECTOR_H

#include <iostream>

class Vector2d {
private:
    float x;
    float y;
public:
    Vector2d(){

    };

    Vector2d(float x,float y):x(x),y(y){
    }

    Vector2d&& add(Vector2d& other){
        this->x += other.x;
        this->y += other.y;
        Vector2d newVector{this->x,this->y};
        return std::move(newVector);
    }
    Vector2d&& subtract(Vector2d& other){
        this->x -= other.x;
        this->y -= other.y;
        Vector2d newVector{this->x,this->y};
        return std::move(newVector);
    }
    Vector2d&& multiply(Vector2d& other){
        this->x *= other.x;
        this->y *= other.y;
        Vector2d newVector{this->x,this->y};
        return std::move(newVector);
    }
    Vector2d&& divide(Vector2d& other){
        this->x /= other.x;
        this->y /= other.y;
        Vector2d newVector{this->x,this->y};
        return std::move(newVector);
    }
    int getX(){
        return (int)x;
    }
    int getY(){
        return (int)y;
    }

};


#endif //GAMEENGINE_VECTOR_H

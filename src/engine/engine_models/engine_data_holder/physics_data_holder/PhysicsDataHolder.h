//
// Created by Filip Ćaćić on 10/01/2019.
//

#ifndef GAMEENGINE_PHYSICSDATAHOLDER_H
#define GAMEENGINE_PHYSICSDATAHOLDER_H


#include <utility>

class PhysicsDataHolder{
protected:

public:
    PhysicsDataHolder(){};
    PhysicsDataHolder(PhysicsDataHolder& pyshicsDataHolder){

    };
    ~PhysicsDataHolder(){

    }

    int measureUnitDistance = 1;
    int measureUnitTime = 1;
    int measureForUnitTimePassed = 0;


    int massOfObject = 0;
    int directionX = 0;
    int directionY = 0;
    float speedX = 0;
    float speedY = 0;
    float accelerationX = 0;
    float accelerationY = 0;








};

#endif //GAMEENGINE_PHYSICSDATAHOLDER_H

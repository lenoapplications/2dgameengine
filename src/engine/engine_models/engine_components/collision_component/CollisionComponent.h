//
// Created by Filip Ćaćić on 22/01/2019.
//

#ifndef GAMEENGINE_COLLISIONCOMPONENT_H
#define GAMEENGINE_COLLISIONCOMPONENT_H


#include <engine/engine_models/entity_component_system/component_model/ComponentModel.h>
#include <engine/interfaces/collision_detector_model/CollisionDetectorInterface.h>
#include <vector>

class BoundingVolumeModel;

class CollisionComponent : public ComponentModel {
private:
    CollisionDetectorInterface* collisionDetectorInterface;
    BoundingVolumeModel* boundingVolumeModel;

protected:

public:

    CollisionComponent(BoundingVolumeModel* boundingVolumeModel,CollisionDetectorInterface* collisionDetectorInterface);
    ~CollisionComponent();

    BoundingVolumeModel* getBoundingVolumeModel();
    void addNewEntityToCheckCollisions(EntityModel* entityModel);


    void init();
    void update();
    void draw();

};


#endif //GAMEENGINE_COLLISIONCOMPONENT_H

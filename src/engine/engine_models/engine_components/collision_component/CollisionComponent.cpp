//
// Created by Filip Ćaćić on 22/01/2019.
//

#include "CollisionComponent.h"

void CollisionComponent::init() {
    collisionDetectorInterface->setEntityModel(entity);
}

void CollisionComponent::update() {
}

void CollisionComponent::draw() {

}

CollisionComponent::CollisionComponent(BoundingVolumeModel* boundingVolumeModel,CollisionDetectorInterface *collisionDetectorInterface):collisionDetectorInterface(collisionDetectorInterface) {
   this->boundingVolumeModel = boundingVolumeModel;
}

CollisionComponent::~CollisionComponent() {
    delete boundingVolumeModel;
}

BoundingVolumeModel *CollisionComponent::getBoundingVolumeModel() {
    return this->boundingVolumeModel;
}

void CollisionComponent::addNewEntityToCheckCollisions(EntityModel *entityModel) {
    collisionDetectorInterface->detectCollision(entity->getComponentHolder(),entityModel);
}


//
// Created by Filip Ćaćić on 26/01/2019.
//

#ifndef GAMEENGINE_SPHERICALBOUND_H
#define GAMEENGINE_SPHERICALBOUND_H


#include <engine/engine_models/engine_components/collision_component/bounding_volumes/bounding_volume_model/BoundingVolumeModel.h>

class SphericalBound : public BoundingVolumeModel {
private:
    float radius;

protected:

public:
    explicit SphericalBound(EntityModel* entityModel);
    ~SphericalBound();

    float getRadius();

    Vector2d test();




};


#endif //GAMEENGINE_SPHERICALBOUND_H

//
// Created by Filip Ćaćić on 26/01/2019.
//

#include "SphericalBound.h"

SphericalBound::SphericalBound(EntityModel *entityModel) : BoundingVolumeModel(entityModel) {
    radius = getImageHypotenuses() / 2;

}

SphericalBound::~SphericalBound() {
}

Vector2d SphericalBound::test() {
    Vector2d vector2d = getCenter();
    int x = static_cast<int>((vector2d.getX() + radius))-engineDataHolder->graphicDataHolder->spaceBetweenVolumeAndImageX;
    int y = static_cast<int>((vector2d.getY() + radius))-engineDataHolder->graphicDataHolder->spaceBetweenVolumeAndImageY;

    return Vector2d(x,y);
}

float SphericalBound::getRadius() {
    return radius;
}

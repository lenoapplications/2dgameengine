//
// Created by Filip Ćaćić on 26/01/2019.
//

#ifndef GAMEENGINE_BOUNDINGVOLUMEMODEL_H
#define GAMEENGINE_BOUNDINGVOLUMEMODEL_H


#include <engine/engine_models/entity_component_system/entity_model/EntityModel.h>

class BoundingVolumeModel {
private:

protected:
    EngineDataHolder* engineDataHolder;


    int getImageHypotenuses(){
        int imageWidth = engineDataHolder->graphicDataHolder->onScreenWidth;
        int imageHeight = engineDataHolder->graphicDataHolder->onScreenHeight;
        return static_cast<int>(sqrt((imageWidth * imageWidth) + (imageHeight * imageHeight)));
    }

public:
    BoundingVolumeModel(EntityModel* entityModel):engineDataHolder(entityModel->getEngineDataHolder()){};
    ~BoundingVolumeModel(){};

    Vector2d getCenter(){
        int xPosition = engineDataHolder->graphicDataHolder->positionOnScreen->getX();
        int yPosition = engineDataHolder->graphicDataHolder->positionOnScreen->getY();
        int imageWidth = ( engineDataHolder->graphicDataHolder->onScreenWidth / 2 ) + xPosition;
        int imageHeight = ( engineDataHolder->graphicDataHolder->onScreenHeight / 2 ) + yPosition;
        return std::move(Vector2d(imageWidth,imageHeight));
    }
};


#endif //GAMEENGINE_BOUNDINGVOLUMEMODEL_H

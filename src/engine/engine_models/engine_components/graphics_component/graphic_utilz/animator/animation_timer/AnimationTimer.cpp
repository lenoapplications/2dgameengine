//
// Created by Filip Ćaćić on 10/01/2019.
//

#include "AnimationTimer.h"
#include <iostream>

AnimationTimer::AnimationTimer() {

}
AnimationTimer::AnimationTimer(GraphicDataHolder* graphicDataHolder) : graphicDataHolder(graphicDataHolder) {

}

AnimationTimer::~AnimationTimer() {

}


bool AnimationTimer::isTimeForNextFrame() {
    if (animationOff()){
        return true;
    }

    else if (graphicDataHolder->currentAnimationTimePassed == graphicDataHolder->animationTimeBetweenFrames){
        reset();
        return true;

    }else{
        return false;
    }
}

void AnimationTimer::reset() {
    graphicDataHolder->currentAnimationTimePassed = 0;
}

bool AnimationTimer::animationOff() {
    return graphicDataHolder->animationTimeBetweenFrames == -1;
}

void AnimationTimer::increaseFrameTime() {
    if(!animationOff()){
        ++graphicDataHolder->currentAnimationTimePassed;
    }
}




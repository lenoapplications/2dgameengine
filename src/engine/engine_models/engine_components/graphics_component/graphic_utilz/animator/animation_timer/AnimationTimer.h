//
// Created by Filip Ćaćić on 10/01/2019.
//

#ifndef GAMEENGINE_ANIMATIONTIMER_H
#define GAMEENGINE_ANIMATIONTIMER_H


#include <engine/engine_models/engine_components/graphics_component/graphic_utilz/graphic_data_holder/GraphicDataHolder.h>

class AnimationTimer {
private:
    GraphicDataHolder* graphicDataHolder;

    void reset();
    bool animationOff();
protected:
    bool isTimeForNextFrame();
    void increaseFrameTime();


public:
    AnimationTimer();
    AnimationTimer(GraphicDataHolder* graphicDataHolder);
    ~AnimationTimer();

};


#endif //GAMEENGINE_ANIMATIONTIMER_H

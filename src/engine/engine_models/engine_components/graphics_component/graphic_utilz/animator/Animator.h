//
// Created by Filip Ćaćić on 10/01/2019.
//

#ifndef GAMEENGINE_ANIMATOR_H
#define GAMEENGINE_ANIMATOR_H
#include<SDL2/SDL.h>
#include<iostream>
#include <engine/engine_models/engine_components/graphics_component/graphic_utilz/animator/animation_timer/AnimationTimer.cpp>
#include <engine/engine_models/engine_components/graphics_component/graphic_utilz/graphic_data_holder/GraphicDataHolder.h>

class SpriteComponent;

template<typename C>
class AnimatorInterface : public AnimationTimer{
private:
    SDL_Rect* srcRect;
    GraphicDataHolder* graphicDataHolder;

    bool animationStarted = false;

    void setupsrcRect(){
        srcRect = new SDL_Rect();
        srcRect->x = graphicDataHolder->imageXPosition;
        srcRect->y = graphicDataHolder->imageYPosition;
        srcRect->w = graphicDataHolder->imageOneFrameWidth;
        srcRect->h = graphicDataHolder->imageOneFrameHeight;
    }

    void nextFrameXDirection(){
        graphicDataHolder->imageXPosition = graphicDataHolder->imageOneFrameWidth * graphicDataHolder->currentIndexOfAnimationFrame;
        ++graphicDataHolder->currentIndexOfAnimationFrame;
        checkFrameAnimationStatus();
    }

    void checkFrameAnimationStatus(){
        if ( graphicDataHolder->currentIndexOfAnimationFrame == graphicDataHolder->animationFramesNumber){
            graphicDataHolder->currentIndexOfAnimationFrame = 0;
        }
    }

public:
    AnimatorInterface(){
        srcRect = nullptr;
    };
    AnimatorInterface(GraphicDataHolder* graphicDataHolder):graphicDataHolder(graphicDataHolder),AnimationTimer(graphicDataHolder){
        setupsrcRect();
    };
    virtual ~AnimatorInterface(){
        std::cout<<"animator interface deleted "<<std::endl;
        delete srcRect;
    };

    void startAnimation(){
        animationStarted = true;
    }
    void stopAnimation(){
        animationStarted = false;
        graphicDataHolder->currentIndexOfAnimationFrame = 0;
        graphicDataHolder->imageXPosition = 0;

    }

    void updateAnimation(){
        if (animationStarted){
            if (isTimeForNextFrame()){
                nextFrameXDirection();
            }else{
                increaseFrameTime();
            }
        }
    }

protected:

    void updateSrcRect(){
        srcRect->x = graphicDataHolder->imageXPosition;
        srcRect->w = graphicDataHolder->imageOneFrameWidth;
        srcRect->h = graphicDataHolder->imageOneFrameHeight;
    }

    SDL_Rect* getSrcRect(){
        return srcRect;
    }

};

template<typename A>
class Animator :public AnimatorInterface<A> {
private:

public:


protected:

};

template<>
class Animator<SpriteComponent>:public AnimatorInterface<SpriteComponent>{
private:

public:
    Animator():AnimatorInterface(){};
    Animator(GraphicDataHolder* graphicDataHolder):AnimatorInterface(graphicDataHolder){};
    ~Animator(){};

protected:
};


#endif //GAMEENGINE_ANIMATOR_H

//
// Created by Filip Ćaćić on 10/01/2019.
//

#ifndef GAMEENGINE_GRAPHICDATAHOLDER_H
#define GAMEENGINE_GRAPHICDATAHOLDER_H

#include <iostream>
#include <engine/engine_models/engine_data_holder/physics_data_holder/vector/Vector2d.h>

class GraphicDataHolder{
public:
    GraphicDataHolder(){
        positionOnScreen = new Vector2d(0,0);
    };
    GraphicDataHolder(GraphicDataHolder& copy){
        std::cout<<"DELITING DATA HOLDER "<<std::endl;

        this->positionOnScreen = copy.positionOnScreen;
        this-> imageXPosition = copy.imageXPosition;
        this-> imageYPosition = copy.imageYPosition;

        onScreenWidth = copy.onScreenWidth;
        onScreenHeight = copy.onScreenHeight;
        imageWidth = copy.imageWidth;
        imageHeight = copy.imageHeight;
        imageOneFrameWidth = copy.imageOneFrameWidth;
        imageOneFrameHeight = copy.imageOneFrameHeight;

        animationFramesNumber = copy.animationFramesNumber;
        animationTimeBetweenFrames = copy.animationTimeBetweenFrames;
        currentAnimationTimePassed = copy.currentAnimationTimePassed;
        currentIndexOfAnimationFrame = copy.currentIndexOfAnimationFrame;

    };

    ~GraphicDataHolder(){
        std::cout<<"DELITING DATA HOLDER "<<std::endl;
        delete positionOnScreen;
    };

    Vector2d* positionOnScreen;
    int imageXPosition = 0;
    int imageYPosition = 0;


    int onScreenWidth = 0;
    int onScreenHeight = 0;
    int imageWidth = 0;
    int imageHeight = 0;
    int imageOneFrameWidth = 0;
    int imageOneFrameHeight = 0;

    int animationFramesNumber = 0;
    int animationTimeBetweenFrames = 0;
    int currentAnimationTimePassed = 0;
    int currentIndexOfAnimationFrame = 0;


    int spaceBetweenVolumeAndImageX = 0;
    int spaceBetweenVolumeAndImageY = 0;


};

#endif //GAMEENGINE_GRAPHICDATAHOLDER_H

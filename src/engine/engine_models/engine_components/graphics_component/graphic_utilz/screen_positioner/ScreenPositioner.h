//
// Created by Filip Ćaćić on 10/01/2019.
//

#ifndef GAMEENGINE_SCREENPOSITIONER_H
#define GAMEENGINE_SCREENPOSITIONER_H


#include <SDL_rect.h>
#include <engine/engine_models/engine_components/graphics_component/graphic_utilz/graphic_data_holder/GraphicDataHolder.h>

class ScreenPositioner {
private:
    SDL_Rect* destRect;
    GraphicDataHolder* graphicDataHolder;
protected:
    void updateDestRect();
public:
    ScreenPositioner();
    explicit ScreenPositioner(GraphicDataHolder* graphicDataHolder);
    ~ScreenPositioner();


    SDL_Rect* getDestRect();
};


#endif //GAMEENGINE_SCREENPOSITIONER_H

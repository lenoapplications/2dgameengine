//
// Created by Filip Ćaćić on 10/01/2019.
//

#include <iostream>
#include "ScreenPositioner.h"

ScreenPositioner::ScreenPositioner() {
    destRect = nullptr;
}
ScreenPositioner::ScreenPositioner(GraphicDataHolder* graphicDataHolder):graphicDataHolder(graphicDataHolder) {
    destRect = new SDL_Rect();
    destRect->x = graphicDataHolder->positionOnScreen->getX();
    destRect->y = graphicDataHolder->positionOnScreen->getY();
    destRect->w = graphicDataHolder->onScreenWidth;
    destRect->h = graphicDataHolder->onScreenHeight;
}



ScreenPositioner::~ScreenPositioner() {
    std::cout<<"deleted screenpositioner"<<std::endl;
    delete destRect;
}

void ScreenPositioner::updateDestRect() {
    destRect->x = graphicDataHolder->positionOnScreen->getX();
    destRect->y = graphicDataHolder->positionOnScreen->getY();
    destRect->w = graphicDataHolder->onScreenWidth;
    destRect->h = graphicDataHolder->onScreenHeight;
}


SDL_Rect *ScreenPositioner::getDestRect() {
    return destRect;
}



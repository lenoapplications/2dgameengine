//
// Created by Filip Ćaćić on 05/01/2019.
//

#include "SpriteComponent.h"

SpriteComponent::SpriteComponent(const char *filePath):Animator(),ScreenPositioner() {
    this->filePath = filePath;
}
SpriteComponent::SpriteComponent(const char *filePath, GraphicDataHolder* graphicDataHolder):Animator(graphicDataHolder),ScreenPositioner(graphicDataHolder) {
    this->filePath = filePath;
}



SpriteComponent::SpriteComponent() {

}



SpriteComponent::~SpriteComponent() {
}

void SpriteComponent::init() {
    this->spriteTexture = this->entity->loadTexture(this->filePath);
}

void SpriteComponent::update() {
    updateSrcRect();
    updateDestRect();
    updateAnimation();
}

void SpriteComponent::draw() {
    this->entity->render(spriteTexture,getSrcRect(),getDestRect());
}



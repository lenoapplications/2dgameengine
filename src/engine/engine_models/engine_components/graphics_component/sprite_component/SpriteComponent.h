//
// Created by Filip Ćaćić on 05/01/2019.
//

#ifndef GAMEENGINE_SPRITECOMPONENT_H
#define GAMEENGINE_SPRITECOMPONENT_H

#include <engine/engine_models/entity_component_system/entity_model/EntityModel.h>
#include <SDL_render.h>
#include <engine/engine_models/engine_components/graphics_component/graphic_utilz/animator/Animator.h>
#include <engine/engine_models/engine_components/graphics_component/graphic_utilz/screen_positioner/ScreenPositioner.cpp>


class SpriteComponent : public ComponentModel,public Animator<SpriteComponent>,ScreenPositioner{
private:
    SDL_Texture* spriteTexture;
    const char* filePath;
protected:

public:
    SpriteComponent();
    SpriteComponent(const char* filePath);
    SpriteComponent(const char* filePath,GraphicDataHolder* graphicDataHolder);
    ~SpriteComponent();



    void init() override;
    void update() override;
    void draw() override;


};


#endif //GAMEENGINE_SPRITECOMPONENT_H

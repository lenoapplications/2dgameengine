//
// Created by Filip Ćaćić on 11/01/2019.
//

#ifndef GAMEENGINE_EVENTCOMPONENT_H
#define GAMEENGINE_EVENTCOMPONENT_H


#include <engine/engine_models/entity_component_system/component_model/ComponentModel.h>
#include <engine/interfaces/event_listener_model/EventListenerInterface.h>

class EventComponent : public ComponentModel{
private:
    EventListenerInterface* eventListenerInterface;
protected:
public:
    EventComponent();
    EventComponent(EventListenerInterface* eventListenerInterface);
    ~EventComponent();
    void init();
    void update();
    void draw();

};


#endif //GAMEENGINE_EVENTCOMPONENT_H

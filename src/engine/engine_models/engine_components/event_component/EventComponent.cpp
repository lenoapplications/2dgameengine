//
// Created by Filip Ćaćić on 11/01/2019.
//

#include "EventComponent.h"


EventComponent::EventComponent() {

}
EventComponent::EventComponent(EventListenerInterface *eventListenerInterface):eventListenerInterface(eventListenerInterface) {

}

EventComponent::~EventComponent() {
    delete eventListenerInterface;
}

void EventComponent::init() {
}

void EventComponent::update() {
    eventListenerInterface->handleEvent(entity->getSdlEvent(), entity->getComponentHolder());
}

void EventComponent::draw() {

}


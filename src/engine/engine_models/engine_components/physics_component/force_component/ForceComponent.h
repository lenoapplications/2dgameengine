//
// Created by Filip Ćaćić on 04/02/2019.
//

#ifndef GAMEENGINE_FORCECOMPONENT_H
#define GAMEENGINE_FORCECOMPONENT_H


#include <engine/engine_models/entity_component_system/component_model/ComponentModel.h>
#include <engine/engine_models/engine_components/physics_component/physics_utilz/kinematic/KinematicFunctions.h>
#include <engine/engine_models/engine_components/physics_component/physics_utilz/forces/Force.h>
#include <utilz/math_functions/MathUtilz.h>


class ForceComponent :public ComponentModel {
private:
    KinematicFunctions* kinematicFunctions;
    EngineDataHolder* engineDataHolder;
    Force* force;

    void updateKinematic();
    float updatePositionX();
    float updatePositionY();
    float convertToDefinedDistanceUnit(float result);
    void checkIfAcceleratingX();
    void checkIfAcceleratingY();
    void checkMeasuredUnitTime(Vector2d& vector2d);

protected:
public:
    ForceComponent(EngineDataHolder* engineDataHolder);
    ~ForceComponent();
    void init() override;
    void update() override;
    void draw() override;



    Force* getForce();
};


#endif //GAMEENGINE_FORCECOMPONENT_H

//
// Created by Filip Ćaćić on 04/02/2019.
//

#include "ForceComponent.h"

ForceComponent::ForceComponent(EngineDataHolder* engineDataHolder) : engineDataHolder(engineDataHolder){
    kinematicFunctions = new KinematicFunctions(engineDataHolder->physicsDataHolder);
    force = new Force(kinematicFunctions,engineDataHolder->physicsDataHolder);

}

ForceComponent::~ForceComponent() {
    delete kinematicFunctions;
}


Force *ForceComponent::getForce() {
    return force;
}




void ForceComponent::init() {

}
void ForceComponent::update(){
    updateKinematic();
}
void ForceComponent::draw() {

}






void ForceComponent::updateKinematic() {
    if (kinematicFunctions->isObjectMoving()){
        float newXPosition = updatePositionX();
        float newYPosition = updatePositionY();

        if(kinematicFunctions->isAccelerating()){
            checkIfAcceleratingX();
            checkIfAcceleratingY();
        }
        Vector2d newPosition{newXPosition,newYPosition};
        checkMeasuredUnitTime(newPosition);
        *engineDataHolder->graphicDataHolder ->positionOnScreen = engineDataHolder->graphicDataHolder->positionOnScreen->add(newPosition);
    }
}

float ForceComponent::updatePositionY() {
    int directionY = engineDataHolder->physicsDataHolder->directionY;
    float speedY = engineDataHolder->physicsDataHolder->speedY;
    float result = speedY * directionY;
    return convertToDefinedDistanceUnit(result);}

float ForceComponent::updatePositionX() {
    float speedX = engineDataHolder->physicsDataHolder->speedX;
    return convertToDefinedDistanceUnit(speedX);
}

float ForceComponent::convertToDefinedDistanceUnit(float result) {
    return result * engineDataHolder->physicsDataHolder->measureUnitDistance;
}

void ForceComponent::checkIfAcceleratingX() {
    if (kinematicFunctions->checkIfAcceleratingX()){
        int directionX = engineDataHolder->physicsDataHolder->directionX;
        float accelerationX = engineDataHolder->physicsDataHolder->accelerationX;
        engineDataHolder->physicsDataHolder->speedX += (accelerationX * directionX);
    }

}
void ForceComponent::checkIfAcceleratingY() {
    if (kinematicFunctions->checkIfAcceleratingY()){
        engineDataHolder->physicsDataHolder->speedY += engineDataHolder->physicsDataHolder->accelerationY;
    }
}

void ForceComponent::checkMeasuredUnitTime(Vector2d& vector2d) {
    if (kinematicFunctions->timePassedMatchesMeasureUnitTame()){
        kinematicFunctions->resetTimePassed();
    }else{
        kinematicFunctions->increaseMesauredTimePassed();
    }
}



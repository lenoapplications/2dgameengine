//
// Created by Filip Ćaćić on 05/02/2019.
//

#include <utilz/math_functions/MathUtilz.h>
#include "ForceApplier.h"

ForceApplier::ForceApplier() {

}

ForceApplier::~ForceApplier() {

}


void ForceApplier::calculateAccelerationXForAppliedForce(KinematicFunctions *kinematicFunctions,int mass ,int force) {
    if (!kinematicFunctions->isObjectMovingX()){
        kinematicFunctions->startAcceleratingX();
        kinematicFunctions->initSpeedX();
    }
    kinematicFunctions->setAccelerationX((calculateAccelerationFromForceAndMass(mass, force)));
}


void ForceApplier::calculateAccelerationYForAppliedForce(KinematicFunctions *kinematicFunctions, int mass, int force) {
    if (!kinematicFunctions->isObjectMovingY()){
        kinematicFunctions->startAcceleratingY();
        kinematicFunctions->initSpeedY();
    }
    kinematicFunctions->setAccelerationY((calculateAccelerationFromForceAndMass(mass, force)));
}


float ForceApplier::calculateAccelerationFromForceAndMass(float mass, float force) {
   float value =  turnFloatToIntegralOrHalf(force/mass);
   return value;
}


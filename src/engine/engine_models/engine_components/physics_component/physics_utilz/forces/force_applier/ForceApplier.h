//
// Created by Filip Ćaćić on 05/02/2019.
//

#ifndef GAMEENGINE_FORCEAPPLIER_H
#define GAMEENGINE_FORCEAPPLIER_H


#include <engine/engine_models/engine_components/physics_component/physics_utilz/kinematic/KinematicFunctions.h>
#include <math.h>

class ForceApplier {
private:
    float calculateAccelerationFromForceAndMass(float mass,float force);
protected:
public:
    ForceApplier();
    ~ForceApplier();

    void calculateAccelerationXForAppliedForce(KinematicFunctions* physicsDataHolder,int mass,int force);
    void calculateAccelerationYForAppliedForce(KinematicFunctions* physicsDataHolder,int mass,int force);

};


#endif //GAMEENGINE_FORCEAPPLIER_H

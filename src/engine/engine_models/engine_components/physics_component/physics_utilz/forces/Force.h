//
// Created by Filip Ćaćić on 04/02/2019.
//

#ifndef GAMEENGINE_FORCE_H
#define GAMEENGINE_FORCE_H


#include <engine/engine_models/engine_components/physics_component/physics_utilz/forces/force_applier/ForceApplier.cpp>

class Force{
private:
    KinematicFunctions* kinematicFunctions;
    PhysicsDataHolder* physicsDataHolder;
    ForceApplier* forceApplier;


    void applyForceX(int force);
    void applyForceY(int force);

protected:

public:
    Force(KinematicFunctions* kinematicFunctions,PhysicsDataHolder* physicsDataHolder);
    ~Force();

    void applyForceWest(int force);
    void applyForceEast(int force);
    void applyForceNorth(int force);
    void applyForceSouth(int force);

    void stopAcceleratingX();
    void stopAcceleratingY();

    bool isObjectMoving();

    float getForceOfObjectIsMoving();
};



Force::Force(KinematicFunctions *kinematicFunctions, PhysicsDataHolder* physicsDataHolder):kinematicFunctions(kinematicFunctions),physicsDataHolder(physicsDataHolder) {
    forceApplier = new ForceApplier();
}
Force::~Force() {
    delete forceApplier;
}


void Force::applyForceEast(int force) {
    kinematicFunctions->goEast();
    applyForceX(force);
}

void Force::applyForceWest(int force) {
    kinematicFunctions->goWest();
    applyForceX(force);
}
void Force::applyForceNorth(int force) {
    kinematicFunctions->goNorth();
    applyForceY(force);
}

void Force::applyForceSouth(int force) {
    kinematicFunctions->goSouth();
    applyForceY(force);
}

void Force::applyForceX(int force) {
    forceApplier->calculateAccelerationXForAppliedForce(kinematicFunctions,physicsDataHolder->massOfObject,force);
}

void Force::applyForceY(int force) {
    forceApplier->calculateAccelerationYForAppliedForce(kinematicFunctions,physicsDataHolder->massOfObject,force);

}

void Force::stopAcceleratingX() {
    kinematicFunctions->stopAcceleratingX();
}

void Force::stopAcceleratingY() {
    kinematicFunctions->stopAcceleratingY();
}

bool Force::isObjectMoving() {
    return kinematicFunctions->isObjectMovingX();
}

float Force::getForceOfObjectIsMoving() {
    return physicsDataHolder->accelerationX * physicsDataHolder->massOfObject;
}


#endif //GAMEENGINE_FORCE_H

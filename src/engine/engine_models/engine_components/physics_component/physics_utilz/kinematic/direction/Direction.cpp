//
// Created by Filip Ćaćić on 10/01/2019.
//

#include "Direction.h"

Direction::Direction(PhysicsDataHolder *pyshicsDataHolder):pyshicsDataHolder(pyshicsDataHolder) {

}

Direction::~Direction() {

}

void Direction::moveSouth() {
    pyshicsDataHolder->directionY = 1;
}

void Direction::moveNorth() {
    pyshicsDataHolder->directionY = -1;
}

void Direction::moveWest() {
    pyshicsDataHolder->directionX = -1;
}

void Direction::moveEast() {
    pyshicsDataHolder->directionX = 1;

}




//
// Created by Filip Ćaćić on 10/01/2019.
//

#ifndef GAMEENGINE_DISPLACEMENT_H
#define GAMEENGINE_DISPLACEMENT_H
#include<engine/engine_models/engine_data_holder/physics_data_holder/PhysicsDataHolder.h>

class Direction {
private:
    PhysicsDataHolder* pyshicsDataHolder;
protected:
public:
    Direction(PhysicsDataHolder* pyshicsDataHolder);
    ~Direction();

    void moveEast();
    void moveWest();
    void moveNorth();
    void moveSouth();

};


#endif //GAMEENGINE_DISPLACEMENT_H

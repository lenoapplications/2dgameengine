//
// Created by Filip Ćaćić on 10/01/2019.
//

#include "Acceleration.h"

Acceleration::Acceleration() {
    acceleratingX = false;
    acceleratingY = false;
}

Acceleration::Acceleration(PhysicsDataHolder *physicsDataHolder):physicsDataHolder(physicsDataHolder)  {
    acceleratingX = false;
    acceleratingY = false;
}


Acceleration::~Acceleration() {

}

void Acceleration::accelerateX() {
    acceleratingX = true;
}
void Acceleration::accelerateY() {
    acceleratingY = true;
}

void Acceleration::stopAccelerationY() {
    acceleratingY = false;
}

void Acceleration::stopAccelerationX() {
    acceleratingX = false;
}
bool Acceleration::isAcceleratingX() {
    return acceleratingX;
}

bool Acceleration::isAcceleratingY() {
    return acceleratingY;
}




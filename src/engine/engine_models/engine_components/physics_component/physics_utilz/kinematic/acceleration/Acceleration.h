//
// Created by Filip Ćaćić on 10/01/2019.
//

#ifndef GAMEENGINE_VELOCITY_H
#define GAMEENGINE_VELOCITY_H


#include <engine/engine_models/engine_data_holder/physics_data_holder/PhysicsDataHolder.h>

class Acceleration {;
protected:
private:
    bool acceleratingX;
    bool acceleratingY;
    PhysicsDataHolder* physicsDataHolder;
public:
    Acceleration();
    Acceleration(PhysicsDataHolder* physicsDataHolder);
    ~Acceleration();

    bool isAcceleratingX();
    bool isAcceleratingY();
    void accelerateX();
    void accelerateY();
    void stopAccelerationX();
    void stopAccelerationY();


};


#endif //GAMEENGINE_VELOCITY_H

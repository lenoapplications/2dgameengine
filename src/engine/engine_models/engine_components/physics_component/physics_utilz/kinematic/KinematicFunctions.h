//
// Created by Filip Ćaćić on 04/02/2019.
//

#ifndef GAMEENGINE_KINEMATICFUNCTIONS_H
#define GAMEENGINE_KINEMATICFUNCTIONS_H

#include <engine/engine_models/engine_data_holder/physics_data_holder/PhysicsDataHolder.h>
#include <engine/engine_models/engine_components/physics_component/physics_utilz/measurment/Measurment.h>
#include <engine/engine_models/engine_components/physics_component/physics_utilz/kinematic/direction/Direction.cpp>
#include <engine/engine_models/engine_components/physics_component/physics_utilz/kinematic/acceleration/Acceleration.cpp>

class KinematicFunctions{
private:

protected:

public:
private:

protected:
    PhysicsDataHolder *physicsDataHolder;
    Measurment *measurment;
    Direction *direction;
    Acceleration * acceleration;
public:
    KinematicFunctions(PhysicsDataHolder* physicsDataHolder);
    ~KinematicFunctions();

    void goNorth();
    void goSouth();
    void goWest();
    void goEast();

    void setSpeedX(int speedX);
    void setSpeedY(int speedY);
    void stopAcceleratingX();
    void stopAcceleratingY();
    void initSpeedX();
    void initSpeedY();

    bool isAccelerating();
    bool checkIfAcceleratingX();
    bool checkIfAcceleratingY();
    bool isObjectMoving();
    bool isObjectMovingX();
    bool isObjectMovingY();
    bool timePassedMatchesMeasureUnitTame();


    void setAccelerationX(float acceleration);
    void setAccelerationY(float acceleration);
    void startAcceleratingX();
    void startAcceleratingY();

    void resetTimePassed();
    void increaseMesauredTimePassed();
};


KinematicFunctions::KinematicFunctions(PhysicsDataHolder* physicsDataHolder):physicsDataHolder(physicsDataHolder) {
    measurment = new Measurment(physicsDataHolder);
    direction = new Direction(physicsDataHolder);
    acceleration = new Acceleration(physicsDataHolder);
}

KinematicFunctions::~KinematicFunctions() {
    delete measurment;
    delete direction;
    delete acceleration;
}

void KinematicFunctions::goNorth() {
    direction->moveNorth();

}

void KinematicFunctions::goSouth() {
    direction->moveSouth();
}

void KinematicFunctions::goWest() {
    direction->moveWest();
}

void KinematicFunctions::goEast() {
    direction->moveEast();
}

void KinematicFunctions::setSpeedX(int speedX) {
    physicsDataHolder->speedX = speedX;
}

void KinematicFunctions::setSpeedY(int speedY) {
    physicsDataHolder->speedY = speedY;
}

void KinematicFunctions::stopAcceleratingX() {
    acceleration->stopAccelerationX();
}

void KinematicFunctions::stopAcceleratingY() {
    acceleration->stopAccelerationY();
}

void KinematicFunctions::setAccelerationX(float acceleration) {
    physicsDataHolder->accelerationX = acceleration;
}

void KinematicFunctions::setAccelerationY(float acceleration) {
    physicsDataHolder -> accelerationY = acceleration;
}

void KinematicFunctions::startAcceleratingX() {
    acceleration->accelerateX();

}

void KinematicFunctions::startAcceleratingY() {
    acceleration->accelerateY();
}

bool KinematicFunctions::isAccelerating() {
    return acceleration->isAcceleratingX() || acceleration->isAcceleratingY();
}

bool KinematicFunctions::checkIfAcceleratingY() {
    return acceleration->isAcceleratingY();
}

bool KinematicFunctions::checkIfAcceleratingX() {
    return acceleration->isAcceleratingX();
}

bool KinematicFunctions::timePassedMatchesMeasureUnitTame() {
    return measurment->timePassedMatchesMeasureUnitTame();
}

void KinematicFunctions::resetTimePassed() {
    measurment->resetTimePassed();
}

void KinematicFunctions::increaseMesauredTimePassed() {
    measurment->increaseMeasureTimePassed();
}

bool KinematicFunctions::isObjectMoving() {
    return physicsDataHolder->speedX == 0 || physicsDataHolder->speedY == 0;
}

void KinematicFunctions::initSpeedY() {
    physicsDataHolder->speedY = 1;
}

void KinematicFunctions::initSpeedX() {
    physicsDataHolder->speedX = 1;
}

bool KinematicFunctions::isObjectMovingY() {
    return physicsDataHolder->accelerationY > 0;
}

bool KinematicFunctions::isObjectMovingX() {
    return physicsDataHolder->accelerationX > 0;
}


#endif //GAMEENGINE_KINEMATICFUNCTIONS_H

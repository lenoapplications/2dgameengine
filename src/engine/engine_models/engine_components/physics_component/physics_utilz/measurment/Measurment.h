//
// Created by Filip Ćaćić on 10/01/2019.
//

#ifndef GAMEENGINE_MEASURMENT_H
#define GAMEENGINE_MEASURMENT_H


#include <iostream>
#include<engine/engine_models/engine_data_holder/physics_data_holder/PhysicsDataHolder.h>

class Measurment{
private:
    PhysicsDataHolder* physicsDataHolder;


protected:
public:
    Measurment(){};
    Measurment(PhysicsDataHolder* physicsDataHolder):physicsDataHolder(physicsDataHolder){};
    ~Measurment(){};

    void changeMeasureUnitTime(int frameUnit);
    void changeMeasuereUnitDistance(int pixelUnit);

    bool timePassedMatchesMeasureUnitTame();
    void resetTimePassed();
    void increaseMeasureTimePassed();
};

void Measurment::changeMeasuereUnitDistance(int distance) {
    this->physicsDataHolder->measureUnitDistance = distance;
}

void Measurment::changeMeasureUnitTime(int time) {
    this->physicsDataHolder->measureUnitTime = time;
}

bool Measurment::timePassedMatchesMeasureUnitTame() {
    return physicsDataHolder ->measureForUnitTimePassed == physicsDataHolder -> measureUnitTime;
}

void Measurment::resetTimePassed() {
    physicsDataHolder->measureForUnitTimePassed = 0;

}

void Measurment::increaseMeasureTimePassed() {
    ++physicsDataHolder->measureForUnitTimePassed;

}

#endif //GAMEENGINE_MEASURMENT_H

cmake_minimum_required(VERSION 3.12)
project(gameEngine)

set(CMAKE_CXX_STANDARD 14)
add_executable(gameEngine main.cpp)

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")

target_link_libraries(gameEngine ${CMAKE_SOURCE_DIR}/libs/threadWatcher/lib/libthreadWatcher.a)
include_directories(${CMAKE_SOURCE_DIR}/libs/threadWatcher/include)

include_directories("${PROJECT_SOURCE_DIR}/src/.")
find_package(SDL2 REQUIRED)
find_package(SDL2_image REQUIRED)
include_directories(${SDL2_INCLUDE_DIR} ${SDL2_IMAGE_INCLUDE_DIR} ${CaveStory_SOURCE_DIR}/source/headers)
target_link_libraries(gameEngine ${SDL2_LIBRARY} ${SDL2_IMAGE_LIBRARIES})



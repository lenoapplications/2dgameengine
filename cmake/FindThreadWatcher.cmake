set(FIND_THREADWATCHER_PATHS
        /libs/threadWatcher)

find_path(THREADWATCHER_INCLUDE_DIR library.h
        PATH_SUFFIXES include
        PATHS ${FIND_THREADWATCHER_PATHS})

find_library(THREADWATCHER_LIBRARY
            NAMES libthreadWatcher
            PATH_SUFFIXES lib
            PATH ${FIND_THREADWATCHER_PATHS})

//
// Created by Filip Ćaćić on 07/02/2019.
//

#ifndef THREADWATCHER_THREADSTATUSHOLDER_H
#define THREADWATCHER_THREADSTATUSHOLDER_H


#include <Flags.h>
#include <DataHolder.h>

class ThreadStatusHolder {
private:
    Flags flags{};
    DataHolder dataHolder;
    bool finished;
protected:
public:
    ThreadStatusHolder();
    ~ThreadStatusHolder();
    void addFlag(std::string id,bool flag);
    bool containsFlag(std::string id);
    void removeFlag(std::string id);
    bool getFlag(std::string id);
    void waitForFlag(std::string id);
    void finish();

    void addObject(AbstractDataWrapper* dataWrapper);

    bool doesObjectExist(std::string id);

    void removeObject(std::string id);

    template<typename T>
    DataWrapper<T>* getObject(std::string id);
    template<typename T>
    DataWrapper<T>&& getObjectAndRemoveIt(std::string id);

    bool isFinished();
};
ThreadStatusHolder::ThreadStatusHolder() {
    finished = false;
}

ThreadStatusHolder::~ThreadStatusHolder() {
}

void ThreadStatusHolder::addFlag(std::string id, bool flag) {
    flags.addFlag(id,flag);
}

bool ThreadStatusHolder::containsFlag(std::string id) {
    return flags.checkIfIdExists(id);
}

void ThreadStatusHolder::removeFlag(std::string id) {
    flags.removeFlag(id);
}

bool ThreadStatusHolder::getFlag(std::string id) {
    return flags.getFlag(id);
}

void ThreadStatusHolder::finish() {
    finished = true;
}

bool ThreadStatusHolder::isFinished() {
    return finished;
}


void ThreadStatusHolder::addObject(AbstractDataWrapper* dataWrapper) {
    dataHolder.addData(dataWrapper);
}

template<typename T>
DataWrapper<T>* ThreadStatusHolder::getObject(std::string id) {
    return dataHolder.getData<T>(id);
}

template<typename T>
DataWrapper<T> &&ThreadStatusHolder::getObjectAndRemoveIt(std::string id) {
    DataWrapper<T>* data = dataHolder.getData<T>(id);
    DataWrapper<T>&& rvalue = std::move(*data);
    dataHolder.removeData(id);
    return std::move(rvalue);
}

void ThreadStatusHolder::waitForFlag(std::string id) {
    while(!flags.checkIfIdExists(id)){};
}

void ThreadStatusHolder::removeObject(std::string id) {
    dataHolder.removeData(id);
}

bool ThreadStatusHolder::doesObjectExist(std::string id) {
    return dataHolder.dataExist(id);
}


#endif //THREADWATCHER_THREADSTATUSHOLDER_H

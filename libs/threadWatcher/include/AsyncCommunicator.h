//
// Created by Filip Ćaćić on 07/02/2019.
//

#ifndef THREADWATCHER_ASYNCCOMMUNICATOR_H
#define THREADWATCHER_ASYNCCOMMUNICATOR_H

#include <iostream>
#include <ThreadIdentHolder.h>
#include <ThreadCreator.h>

class AsyncCommunicator{
private:
    static AsyncCommunicator* asyncCommunicator;
    ThreadIdentHolder threadIdentHolder;
    ThreadCreator threadCreator;

    std::map<std::string,std::thread::id> threads;

    bool checkIfThreadExists(std::map<std::string,std::thread::id>::iterator* it,std::string threadName);

protected:
public:
    static AsyncCommunicator* getAsyncCommunicator(){
        return asyncCommunicator;
    }

    std::thread::id&& startNewThread(std::thread* threadToStart);
    void saveThreadId(std::thread::id threadId);
    void saveThreadToMap(std::thread::id id,std::string threadName);
    std::thread::id* getThreadId(std::string key);
    template<typename T>
    T* getObject(std::thread::id threadId, std::string objectId);
    void removeObject(std::thread::id id,std::string objectId);

    bool doesObjectExist(std::thread::id,std::string objectId);

    template<typename T>
    void addObjectToThreadId(std::thread::id threadId,std::string objectId,T* object);
    bool waitThreadToFinish(std::thread::id threadId);
    template<typename T>
    T* waitForThreadToAddObject(std::thread::id threadId,std::string id);
    template<typename T>
    T&& waitForThreadToAddObjectAndRemoveItFromList(std::thread::id,std::string id);



    bool waitForFlagToShow(std::thread::id,std::string flagId);
    bool getFlagStatus(std::thread::id threadId,std::string flagId);
    void addFlag(std::thread::id threadId,std::string flagId,bool status);

};

AsyncCommunicator* AsyncCommunicator::asyncCommunicator = new AsyncCommunicator();



std::thread::id&& AsyncCommunicator::startNewThread(std::thread* threadToStart) {
    std::thread::id id = threadCreator.initThread(threadToStart);
    threadIdentHolder.addThread(id);
    threadToStart->detach();
    return std::move(id);
}
void AsyncCommunicator::saveThreadId(std::thread::id threadId) {
    threadIdentHolder.addThread(threadId);
}

void AsyncCommunicator::saveThreadToMap(std::thread::id id, std::string threadName) {
    std::map<std::string,std::thread::id>::iterator iterator;
    if (checkIfThreadExists(&iterator,threadName)){
        threads[iterator->first] = id;
    }else{
        threads.insert(std::pair<std::string,std::thread::id>(threadName,id));
    }
}
std::thread::id* AsyncCommunicator::getThreadId(std::string key) {
    std::map<std::string,std::thread::id>::iterator iterator;
    if (checkIfThreadExists(&iterator,key)){
        return &iterator->second;
    }
    return nullptr;
}


bool AsyncCommunicator::waitThreadToFinish(std::thread::id threadId) {
    ThreadStatusHolder* threadStatusHolder = threadIdentHolder.getThreadStatusHolder(threadId);
    if (threadStatusHolder == nullptr){
        return false;
    }
    while(!threadStatusHolder->isFinished()){}
    return true;
}

template<typename T>
T *AsyncCommunicator::waitForThreadToAddObject(std::thread::id threadId,std::string objectId) {
    ThreadStatusHolder* threadStatusHolder = threadIdentHolder.getThreadStatusHolder(threadId);
    if (threadStatusHolder == nullptr){
        return NULL;
    }
    while(threadStatusHolder->getObject<T>(objectId) == nullptr){};
    return threadStatusHolder->getObject<T>(objectId)->getData();
}

template<typename T>
T&& AsyncCommunicator::waitForThreadToAddObjectAndRemoveItFromList(std::thread::id threadId, std::string id) {
    ThreadStatusHolder* threadStatusHolder = threadIdentHolder.getThreadStatusHolder(threadId);
    if ( threadStatusHolder == nullptr){
        return NULL;
    }
    while(threadStatusHolder->getObject<T>(id) == nullptr){};
    DataWrapper<T>&& pointerToData = threadStatusHolder->getObjectAndRemoveIt<T>(id);
    return std::move(pointerToData.getData());
}

template<typename T>
void AsyncCommunicator::addObjectToThreadId(std::thread::id threadId, std::string objectId,T* object) {
    ThreadStatusHolder* threadStatusHolder = threadIdentHolder.getThreadStatusHolder(threadId);
    DataWrapper<T>* dataWrapper = new DataWrapper<T>(object,objectId);
    if (threadStatusHolder != nullptr){
        threadStatusHolder->addObject(dataWrapper);
    }
}


bool AsyncCommunicator::waitForFlagToShow(std::thread::id threadId, std::string flagId) {
    ThreadStatusHolder* threadStatusHolder = threadIdentHolder.getThreadStatusHolder(threadId);
    if (threadStatusHolder != nullptr){
        threadStatusHolder->waitForFlag(flagId);
        return true;
    }
    return false;
}

bool AsyncCommunicator::getFlagStatus(std::thread::id threadId, std::string flagId) {
    ThreadStatusHolder* threadStatusHolder = threadIdentHolder.getThreadStatusHolder(threadId);
    if (threadStatusHolder != nullptr){
        return threadStatusHolder->getFlag(flagId);
    }
    return false;
}

void AsyncCommunicator::addFlag(std::thread::id threadId, std::string flagId,bool status) {
    ThreadStatusHolder* threadStatusHolder = threadIdentHolder.getThreadStatusHolder(threadId);
    if ( threadStatusHolder != nullptr){
        threadStatusHolder->addFlag(flagId,status);
    }
}

bool
AsyncCommunicator::checkIfThreadExists(std::map<std::string, std::thread::id>::iterator *it, std::string threadName) {
    *it = threads.find(threadName);
    return *it != threads.end();
}
template<typename T>
T *AsyncCommunicator::getObject(std::thread::id threadId, std::string objectId) {
    ThreadStatusHolder* threadStatusHolder = threadIdentHolder.getThreadStatusHolder(threadId);
    if ( threadStatusHolder == nullptr){
        return NULL;
    }
    return threadStatusHolder->getObject<T>(objectId)->getData();
}

void AsyncCommunicator::removeObject(std::thread::id id, std::string objectId) {
    ThreadStatusHolder* threadStatusHolder = threadIdentHolder.getThreadStatusHolder(id);
    if ( threadStatusHolder != nullptr){
        threadStatusHolder->removeObject(objectId);
    }
}

bool AsyncCommunicator::doesObjectExist(std::thread::id threadId, std::string objectId) {
    ThreadStatusHolder* threadStatusHolder = threadIdentHolder.getThreadStatusHolder(threadId);
    if ( threadStatusHolder != nullptr){
        return threadStatusHolder->doesObjectExist(objectId);
    }
    return false;
}


#endif //THREADWATCHER_ASYNCCOMMUNICATOR_H

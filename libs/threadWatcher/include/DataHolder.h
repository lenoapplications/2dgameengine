//
// Created by Filip Ćaćić on 07/02/2019.
//

#ifndef THREADWATCHER_DATAHOLDER_H
#define THREADWATCHER_DATAHOLDER_H


#include <vector>
#include <DataWrapper.h>
#include <iostream>

class DataHolder {
private:
    std::vector<AbstractDataWrapper*> dataHolderList;
protected:

public:
    DataHolder();
    ~DataHolder();

    void addData(AbstractDataWrapper* abstractDataWrapper);
    template<typename D>
    DataWrapper<D>* getData(std::string id);
    bool dataExist(std::string id);
    void removeData(std::string id);
};

DataHolder::DataHolder() {

}

DataHolder::~DataHolder() {
    for(AbstractDataWrapper* abstractDataWrapper : dataHolderList){
        delete abstractDataWrapper;
    }
}

void DataHolder::addData(AbstractDataWrapper* abstractDataWrapper) {
    dataHolderList.push_back(abstractDataWrapper);
}

template<typename D>
DataWrapper<D>* DataHolder::getData(std::string id) {
    for (AbstractDataWrapper* abstractDataWrapper : dataHolderList){
        if (abstractDataWrapper->getId() == id){
            return (DataWrapper<D>*)abstractDataWrapper;
        }
    }
    return nullptr;

}

void DataHolder::removeData(std::string id) {
    for (int i = 0; i < dataHolderList.size() ; ++i){
        if (dataHolderList.at(i)->getId() == id){
            dataHolderList.erase(dataHolderList.begin()+i);
            break;
        }
    }
}

bool DataHolder::dataExist(std::string id) {
    for (int i = 0; i < dataHolderList.size() ; ++i){
        if (dataHolderList.at(i)->getId() == id){
            return true;
        }
    }
    return false;
}


#endif //THREADWATCHER_DATAHOLDER_H

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/fcacic/Documents/private/workspace/gitProjects/gameEngine/main.cpp" "/Users/fcacic/Documents/private/workspace/gitProjects/gameEngine/cmake-build-debug/CMakeFiles/gameEngine.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../libs/threadWatcher/include"
  "../src/."
  "/Library/Frameworks/SDL2.framework/Headers"
  "/Library/Frameworks/SDL2_image.framework/Headers"
  "/source/headers"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
